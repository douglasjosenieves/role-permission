#Roles y Permision ui  Laravel 8

# Libreria :
"spatie/laravel-permission": "^4.2"


## Comando a ejecutar
- php artisan migrate
- php artisan db:seed --class=PermissionTableSeeder
- php artisan db:seed --class=RoleTableSeeder
- php artisan db:seed --class=UserTableSeeder


## Usuario administrador credenciales
jiteshmeniya99@gmail.com
12345678     


<img src="https://gitlab.com/douglasjosenieves/role-permission/-/raw/master/public/1.png" width="800">
<img src="https://gitlab.com/douglasjosenieves/role-permission/-/raw/master/public/2.png" width="800">
<img src="https://gitlab.com/douglasjosenieves/role-permission/-/raw/master/public/3.png" width="800">
 

## Desarrollado por: 
*Douglas Nieves*

**Lic. MTI**